# -*- coding: utf-8 -*-
import time
import cv2

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
from torchvision.utils import save_image
import torchvision.transforms as transforms
from PIL import Image
import numpy as np
import math
import time
import os
import datetime
import random
import shutil
from options.test_options import TestOptions
from models import create_model
import torchvision.transforms.functional as F
import torch

import glob
def load_flist(flist):
    # np.genfromtxt(flist, dtype=np.str, encoding='utf-8')
    if isinstance(flist, list):
        return flist
    if isinstance(flist, str):
        if os.path.isdir(flist):
            flist = list(glob.glob(flist + '/*.jpg')) + list(glob.glob(flist + '/*.png'))
            flist.sort()
            return flist

        if os.path.isfile(flist):
            try:
                return np.genfromtxt(flist, dtype=np.str, encoding='utf-8')
            except:

                return [flist]
    return []

def postprocess(img):
  img = (img+1)/2*255
  img = img.permute(0,2,3,1)
  img = img.int().cpu().numpy().astype(np.uint8)
  return img

# Model and version
opt = TestOptions().parse()  # get test options
# hard-code some parameters for test
opt.num_threads = 0   # test code only supports num_threads = 1
opt.model = 'pix2pixglg'
opt.batch_size = 1    # test code only supports batch_size = 1
opt.serial_batches = True  # disable data shuffling; comment this line if results on randomly chosen images are needed.
opt.no_flip = True    # no flip; comment this line if results on flipped images are needed.
opt.display_id = -1   # no visdom display; the test code saves the results to a HTML file.
model = create_model(opt)      # create a model given opt.model and other options
model.setup(opt)               # regular setup: load and print networks; create schedulers

model.eval()


val_image = ''

def Mask_find(image, threshold_factor=3):
    color_image = np.array(image)
    color_image = cv2.cvtColor(color_image, cv2.COLOR_RGB2BGR)
    gray_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)

    gradient_x = cv2.Sobel(gray_image, cv2.CV_64F, 1, 0, ksize=3)
    gradient_y = cv2.Sobel(gray_image, cv2.CV_64F, 0, 1, ksize=3)
    gradient_magnitude = np.sqrt(gradient_x ** 2 + gradient_y ** 2)

    defective_image = np.zeros_like(gray_image)

    mean_magnitude = np.mean(gradient_magnitude)
    std_dev_magnitude = np.std(gradient_magnitude)

    threshold = mean_magnitude + threshold_factor * std_dev_magnitude
    defective_image[gradient_magnitude > threshold] = 255

    damaged_pixels = color_image[gradient_magnitude > threshold]

    channel_means = np.mean(damaged_pixels, axis=0)
    channel_magnitude = np.std(damaged_pixels, axis=0)
    pixel_factor = 2

    for i in range(3):
        channel_defective = color_image[:, :, i] > channel_means[i] + pixel_factor * channel_magnitude[i]
        defective_image[channel_defective] = 255

    defective_image_pil = Image.fromarray(defective_image).convert('L')

    return defective_image_pil

for idx in range(1):
    val_mask = ''
    save_dir = ''
    test_image_flist = load_flist(val_image)
    print(len(test_image_flist))
    test_mask_flist = load_flist(val_mask)
    print(len(test_mask_flist))
    if os.path.exists(save_dir):
        shutil.rmtree(save_dir)
    os.makedirs(os.path.join(save_dir, 'comp'), exist_ok=True)
    os.makedirs(os.path.join(save_dir, 'masked'), exist_ok=True)
    os.makedirs(os.path.join(save_dir, 'orig'), exist_ok=True)
    os.makedirs(os.path.join(save_dir, 'en'), exist_ok=True)
    i = 0
    total = 0.0
    mask_num = len(test_mask_flist)
    # iteration through datasets
    import pandas as pd
    data_list = []
    for idx in range(len(test_image_flist)):
        i += 1
        img = Image.open(test_image_flist[idx]).convert('RGB')
        #mask = Mask_find(img)
        mask = Image.open(test_mask_flist[idx%mask_num]).convert('L')
        masks = F.to_tensor(mask)
        images = F.to_tensor(img)
        images = images.unsqueeze(0)
        masks = masks.unsqueeze(0)
        speed = {'Index': i, 'Execution Time': None}
        data = {'A': images, 'B': masks, 'A_paths': ''}
        model.set_input(data)
        start_time = time.time()
        with torch.no_grad():
            model.forward()
        end_time = time.time()
        execution_time = end_time - start_time
        speed['Execution Time'] = execution_time
        data_list.append(speed)
        total+=execution_time
        print(f"execution_time：{execution_time} s")
        print(idx,total)
        mean = (0.5, 0.5, 0.5)
        std = (0.5, 0.5, 0.5)
        names = test_image_flist[idx].split('/')

        inverse_transform = transforms.Compose([
            transforms.Normalize((-mean[0] / std[0], -mean[1] / std[1], -mean[2] / std[2]),
                                 (1.0 / std[0], 1.0 / std[1], 1.0 / std[2]))
        ])
        save_image(model.images_ip, (save_dir + '/orig/' +names[-1].split('.')[0] + '_orig.png'))
        save_image(inverse_transform(model.images),(save_dir + '/en/' + names[-1].split('.')[0] + '_en.png'))
        save_image(inverse_transform(model.masked_images1), (save_dir + '/masked/' + names[-1].split('.')[0] + '_mask.png'))
        save_image(inverse_transform(model.merged_images3), (save_dir + '/comp/' + names[-1].split('.')[0] + '_comp.png'))
    df = pd.DataFrame(data_list)
    df.to_excel('', index=False)
    avg=total/i
    print('The avg time is',avg)

