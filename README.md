# MER
# MERNet

Deep learning based enhancement and restoration of murals in low-light and defected conditions

## Prerequisites
- Python 3
- NVIDIA GPU + CUDA cuDNN
- PyTorch 1.3.1

## Run
1. train the model
```
python train.py --dataroot no_use --name celebahq_LGNet --model mer --netG1 unet_256 --netG2 resnet_4blocks --netG3 unet256 --netD snpatch --gan_mode lsgan --input_nc 4 --no_dropout --direction AtoB --display_id 0 --gpu_ids 0
```
2. test the model
```
python test_and_save.py --dataroot no_use --name celebahq_LGNet --model mer --netG1 unet_256 --netG2 resnet_4blocks --netG3 unet256 --gan_mode nogan --input_nc 4 --no_dropout --direction AtoB --gpu_ids 0
```


## Acknowledgments
This code borrows from [pytorch-CycleGAN-and-pix2pix](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix), [LG](https://github.com/weizequan/LGNet.git) and [RFR](https://github.com/jingyuanli001/RFR-Inpainting).

