# Our work has been inspired by the following articles
#   @ARTICLE{9730792,
#   author={Quan, Weize and Zhang, Ruisong and Zhang, Yong and Li, Zhifeng and Wang, Jue and Yan, Dong-Ming},
#   journal={IEEE Transactions on Image Processing},
#   title={Image Inpainting With Local and Global Refinement},
#   year={2022},
#   volume={31},
#   pages={2405-2420}
# }
import torch
from .base_model import BaseModel
from . import networks
from .loss import l1_loss_mask, VGG16FeatureExtractor, style_loss, perceptual_loss, TV_loss,LossFunction
import torchvision.transforms as transforms
import torch.utils
import torch.nn as nn

class merModel(BaseModel):
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        parser.set_defaults(norm='batch', netG='unet_256', preprocess='resize', no_dropout=True, load_size=256, is_mask=True, gan_mode='nogan')
        return parser

    def __init__(self, opt):
        """Initialize the pix2pix class.

        Parameters:
            opt (Option class)-- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseModel.__init__(self, opt)
        self.loss_names = ['G', 'G_content', 'G_style', 'G_tv', 'E']

        if self.opt.gan_mode != 'nogan':
            self.loss_names += ['D_real', 'D_fake', 'G_GAN']
        self.visual_names = ['masked_images1', 'merged_images1', 'images', 'merged_images2', 'merged_images3','images_ip']
        self.model_names = ['G1', 'G2', 'G3', 'E', 'C']
        if self.opt.gan_mode != 'nogan':
            self.model_names += ['D']

        self.netE= networks.define_E(layers=1, channels=3,init_type='normal', init_gain=0.02, gpu_ids=self.gpu_ids)
        self.netC = networks.define_C(layers=1, channels=3, init_type='normal', init_gain=0.02, gpu_ids=self.gpu_ids)

        self.netG1 = networks.define_G(opt.input_nc, opt.output_nc, opt.ngf, opt.netG1, opt.norm,
                                      not opt.no_dropout, opt.init_type, opt.init_gain, gpu_ids=self.gpu_ids)
        self.netG2 = networks.define_G(opt.input_nc, opt.output_nc, opt.ngf, opt.netG2, opt.norm,
                                      not opt.no_dropout, opt.init_type, opt.init_gain, gpu_ids=self.gpu_ids)
        self.netG3 = networks.define_G(opt.input_nc, opt.output_nc, opt.ngf, opt.netG3, opt.norm,
                                      not opt.no_dropout, opt.init_type, opt.init_gain, gpu_ids=self.gpu_ids)

        if self.isTrain:
            if opt.gan_mode != 'nogan':
                self.netD = networks.define_D(opt.output_nc, opt.ndf, opt.netD,
                                              opt.n_layers_D, opt.norm, opt.init_type, opt.init_gain, self.gpu_ids)

        if self.isTrain:
            self._criterion = LossFunction()
            self.criterionL1 = torch.nn.L1Loss()

            self.optimizer_G1 = torch.optim.Adam(self.netG1.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
            self.optimizer_G2 = torch.optim.Adam(self.netG2.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
            self.optimizer_G3 = torch.optim.Adam(self.netG3.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
            self.optimizer_E = torch.optim.Adam(self.netE.parameters(), lr=opt.lr, betas=(0.9, 0.999), weight_decay=3e-4)
            self.optimizer_C = torch.optim.Adam(self.netC.parameters(), lr=opt.lr, betas=(0.9, 0.999),weight_decay=3e-4)
            if opt.gan_mode != 'nogan':
                self.criterionGAN = networks.GANLoss(opt.gan_mode).to(self.device)

                self.optimizer_D = torch.optim.Adam(self.netD.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
                self.optimizers.append(self.optimizer_D)

            self.optimizers.append(self.optimizer_G1)
            self.optimizers.append(self.optimizer_G2)
            self.optimizers.append(self.optimizer_G3)
            self.optimizers.append(self.optimizer_E)
            self.optimizers.append(self.optimizer_C)
            self.lossNet = VGG16FeatureExtractor()
            self.lossNet.cuda(opt.gpu_ids[0])

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.

        Parameters:
            input (dict): include the data itself and its metadata information.

        The option 'direction' can be used to swap images in domain A and domain B.
        """
        AtoB = self.opt.direction == 'AtoB'

        self.images_ip = input['A' if AtoB else 'B'].to(self.device)
        self.masks = input['B' if AtoB else 'A'].to(self.device)
        self.image_paths = input['A_paths' if AtoB else 'B_paths']

    def forward(self):
        self.ilist = []
        self.rlist = []
        self.inlist = []
        self.attlist = []
        images_op = self.images_ip
        for i in range(self.opt.stage):
            self.inlist.append(images_op)
            images_en = self.netE(images_op)
            r = self.images_ip / images_en
            r = torch.clamp(r, 0, 1)
            att = self.netC(r)
            images_op = self.images_ip + att
            self.ilist.append(images_en)
            self.rlist.append(r)
            self.attlist.append(torch.abs(att))
        self.images = self.ilist[-1].data
        transform_LG = transforms.Compose([transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
        self.images = transform_LG(self.images)
        self.masked_images1 = self.images * (1 - self.masks) + self.masks
        self.output_images1 = self.netG1(torch.cat((self.masked_images1, self.masks), 1))
        self.merged_images1 = self.images * (1 - self.masks) + self.output_images1 * self.masks
        self.output_images2 = self.netG2(torch.cat((self.merged_images1, self.masks), 1))
        self.merged_images2 = self.images * (1 - self.masks) + self.output_images2 * self.masks
        self.output_images3 = self.netG3(torch.cat((self.merged_images2, self.masks), 1))
        self.merged_images3 = self.images * (1 - self.masks) + self.output_images3 * self.masks
    def backward_E(self):
        self.loss_E = torch.tensor(0.0, requires_grad=True).to(self.device)
        for i in range(self.opt.stage):
            loss_value = self._criterion(self.inlist[i], self.ilist[i]).to(self.device)
            self.loss_E += loss_value
        self.loss_E.backward()

    def backward_D(self):

        pred_fake = self.netD(self.merged_images1.detach()) #预测假


        self.loss_D_fake = self.criterionGAN(pred_fake, False, is_disc=True)

        pred_real = self.netD(self.images)
        self.loss_D_real = self.criterionGAN(pred_real, True, is_disc=True)
        # combine loss and calculate gradients
        self.loss_D = (self.loss_D_fake + self.loss_D_real) * 0.5
        self.loss_D.backward()

    def backward_G(self):
        loss_G_valid = l1_loss_mask(self.output_images1 * (1 - self.masks), self.images * (1 - self.masks),
                                    (1 - self.masks))
        loss_G_hole = l1_loss_mask(self.output_images1 * self.masks, self.images * self.masks, self.masks)
        self.loss_G = loss_G_valid + 6 * loss_G_hole
        loss_G_valid2 = l1_loss_mask(self.output_images2 * (1 - self.masks), self.images * (1 - self.masks),
                                     (1 - self.masks))
        loss_G_hole2 = l1_loss_mask(self.output_images2 * self.masks, self.images * self.masks, self.masks)
        self.loss_G = self.loss_G + loss_G_valid2 + 6 * loss_G_hole2  # 这两类损失加到之前的总损失 self.loss_G 上

        real_B_feats2 = self.lossNet(self.images)
        fake_B_feats2 = self.lossNet(self.output_images2)
        comp_B_feats2 = self.lossNet(self.merged_images2)

        self.loss_G_tv = TV_loss(self.merged_images2 * self.masks)  # 水平和垂直方向上
        self.loss_G_style = style_loss(real_B_feats2, fake_B_feats2) + style_loss(real_B_feats2, comp_B_feats2)
        self.loss_G_content = perceptual_loss(real_B_feats2, fake_B_feats2) + perceptual_loss(real_B_feats2,
                                                                                              comp_B_feats2)  # 感知损失
        self.loss_G = self.loss_G + 0.05 * self.loss_G_content + 120 * self.loss_G_style + 0.1 * self.loss_G_tv
        loss_G_valid3 = l1_loss_mask(self.output_images3 * (1 - self.masks), self.images * (1 - self.masks),
                                     (1 - self.masks))
        loss_G_hole3 = l1_loss_mask(self.output_images3 * self.masks, self.images * self.masks, self.masks)
        self.loss_G = self.loss_G + loss_G_valid3 + 6 * loss_G_hole3

        fake_B_feats3 = self.lossNet(self.output_images3)
        comp_B_feats3 = self.lossNet(self.merged_images3)

        self.loss_G_tv3 = TV_loss(self.merged_images3 * self.masks)
        self.loss_G_style3 = style_loss(real_B_feats2, fake_B_feats3) + style_loss(real_B_feats2, comp_B_feats3)
        self.loss_G_content3 = perceptual_loss(real_B_feats2, fake_B_feats3) + perceptual_loss(real_B_feats2,
                                                                                               comp_B_feats3)
        self.loss_G = self.loss_G + 0.05 * self.loss_G_content3 + 120 * self.loss_G_style3 + 0.1 * self.loss_G_tv3

        if self.opt.gan_mode != 'nogan':
            pred_fake = self.netD(self.merged_images1)  # 判别器结果（即预测值）存储在pred_fake
            self.loss_G_GAN = self.criterionGAN(pred_fake, True, is_disc=False)  # 计算GAN的损失
            self.loss_G = self.loss_G + 0.1 * self.loss_G_GAN
        self.loss_G = self.loss_G.requires_grad_(True)
        self.loss_G.backward(retain_graph=True)

    def optimize_parameters(self, flag):
        self.forward()
        if flag <= 8:
            self.set_requires_grad(self.netD, False)
            self.set_requires_grad([self.netG1, self.netG2, self.netG3], False)
            self.set_requires_grad(self.netE, True)
            self.set_requires_grad(self.netC, True)
            self.optimizer_E.zero_grad()
            self.optimizer_C.zero_grad()
            self.backward_E()
            nn.utils.clip_grad_norm_(self.netE.parameters(), 5)
            nn.utils.clip_grad_norm_(self.netC.parameters(), 5)
            self.optimizer_E.step()
            self.optimizer_C.step()
            self.set_requires_grad(self.netE, False)
            self.set_requires_grad(self.netC, False)
            if flag == 8:
                self.set_requires_grad([self.netG1, self.netG2, self.netG3], True)
                self.set_requires_grad(self.netD, True)
                self.set_requires_grad(self.netE, False)
                self.set_requires_grad(self.netC, False)
        elif flag > 8:
            self.set_requires_grad(self.netE, False)
            self.set_requires_grad(self.netC, False)
            self.set_requires_grad([self.netG1, self.netG2, self.netG3], True)
            if self.opt.gan_mode != 'nogan':
                self.set_requires_grad(self.netD, True)
                self.optimizer_D.zero_grad()
                self.backward_D()
                self.optimizer_D.step()
                self.set_requires_grad(self.netD, False)
            self.set_requires_grad([self.netG1, self.netG2, self.netG3], True)
            self.optimizer_G1.zero_grad()
            self.optimizer_G2.zero_grad()
            self.optimizer_G3.zero_grad()
            self.backward_G()
            self.optimizer_G1.step()
            self.optimizer_G2.step()
            self.optimizer_G3.step()
            self.set_requires_grad([self.netG1, self.netG2, self.netG3], False)
