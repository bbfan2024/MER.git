import os.path
from data.base_dataset import BaseDataset, get_transform
from data.image_folder import make_dataset
from PIL import Image
import random
import torch
import torch.nn.functional as F

class UnalignedDataset(BaseDataset):
    """
    This dataset class can load unaligned/unpaired datasets.

    It requires two directories to host training images from domain A '/path/to/data/trainA'
    and from domain B '/path/to/data/trainB' respectively.
    You can train the model with the dataset flag '--dataroot /path/to/data'.
    Similarly, you need to prepare two directories:
    '/path/to/data/testA' and '/path/to/data/testB' during test time.
    """

    def __init__(self, opt):
        """Initialize this dataset class.

        Parameters:
            opt (Option class) -- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseDataset.__init__(self, opt)
        if opt.phase == 'train':
            self.dir_A = opt.train_imgroot  # create a path '/path/to/data/trainA'
            self.dir_B = opt.train_maskroot  # create a path '/path/to/data/trainB'
        else:
            self.dir_A = opt.test_imgroot  # create a path '/path/to/data/trainA'
            self.dir_B = opt.test_maskroot  # create a path '/path/to/data/trainB'
        self.A_paths = sorted(make_dataset(self.dir_A, opt.max_dataset_size))   # make_dataset 函数的目的是从 self.dir_A 这个文件夹中加载数据集，并且这个数据集的大小不会超过 opt.max_dataset_sizeload images from '/path/to/data/trainA'
        self.B_paths = sorted(make_dataset(self.dir_B, opt.max_dataset_size))   # from data.image_folder import make_datasetload images from '/path/to/data/trainB'
        self.A_size = len(self.A_paths)  # get the size of dataset A
        self.B_size = len(self.B_paths)  # get the size of dataset B
        self.transform_A = get_transform(self.opt, grayscale=(opt.input_nc == 1)) #(opt.input_nc == 1) 是判断输入图像的通道数是否为1
        self.transform_B = get_transform(self.opt, grayscale=True)   #定义transform的内容（方式）

    def __getitem__(self, index):
        """Return a data point and its metadata information.

        Parameters:
             index (int) -- 用于数据索引的随机整数

        返回一个包含 A、B、A_路径和 B_ 路径的字典
            A（张量）-- 输入域中的图像
            B（张量）-- 目标域中的对应图像
            A_paths (str) -- 图像路径
            B_paths (str) -- 图像路径
            index (int)      -- a random integer for data indexing

        Returns a dictionary that contains A, B, A_paths and B_paths
            A (tensor)       -- an image in the input domain
            B (tensor)       -- its corresponding image in the target domain
            A_paths (str)    -- image paths
            B_paths (str)    -- image paths
        """
        A_path = self.A_paths[index % self.A_size]  # 模运算%确保索引值在范围内make sure index is within then range
        if self.opt.serial_batches:   # make sure index is within then range
            index_B = index % self.B_size
        else:   # randomize the index for domain B to avoid fixed pairs.
            index_B = random.randint(0, self.B_size - 1)
        B_path = self.B_paths[index_B]
        A_img = Image.open(A_path).convert('RGB')
        B_img = Image.open(B_path).convert('L')
        # apply image transformation
        A = self.transform_A(A_img)#转换为PyTorch的Tensor格式等操作
        B = self.transform_B(B_img)

        return {'A': A, 'B': B, 'A_paths': A_path, 'B_paths': B_path}

    def __len__(self):
        """Return the total number of images in the dataset.

        As we have two datasets with potentially different number of images,
        we take a maximum of
        """
        return min(self.A_size, self.B_size)
