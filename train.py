import time
from options.train_options import TrainOptions
from data import create_dataset
from models import create_model
from util.visualizer import Visualizer
import numpy as np
import torch.utils
import torch.backends.cudnn as cudnn

if __name__ == '__main__':  #
    opt = TrainOptions().parse()  #
    opt.save_latest_freq = 2000  #
    np.random.seed(opt.seed)  #
    cudnn.benchmark = True  #
    torch.manual_seed(opt.seed)  #
    cudnn.enabled = True  #
    torch.cuda.manual_seed(opt.seed)
    opt.train_imgroot = ''
    opt.train_maskroot = ''
    dataset = create_dataset(opt)  # create a dataset given opt.dataset_mode and other options
    dataset_size = len(dataset)  # get the number of images in the dataset.
    print('The number of training images = %d' % dataset_size)

    model = create_model(opt)  #
    model.setup(opt)  # regular setup: load and print networks; create schedulers
    total_iters = 0  # the total number of training iterations

    model.train()

    for epoch in range(opt.epoch_count, opt.n_epochs + opt.n_epochs_decay + 1):  # outer loop for different epochs; we save the model by <epoch_count>, <epoch_count>+<save_latest_freq>
        epoch_iter = 0  #the number of training iterations in current epoch, reset to 0 every epoch
        for i, data in enumerate(dataset):  #inner loop within one epoch
            total_iters += opt.batch_size  #
            epoch_iter += opt.batch_size
            model.set_input(data)  # unpack data from dataset and apply preprocessing
            model.optimize_parameters(i % 80)
            print(i % 80)
            if total_iters % opt.print_freq == 0:
                losses = model.get_current_losses()
                print(losses)
            if total_iters % opt.save_latest_freq == 0:  # cache our latest model every <save_latest_freq> iterations
                print('saving the latest model (epoch %d, total_iters %d)' % (epoch, total_iters))
                save_suffix = 'iter_%d' % total_iters if opt.save_by_iter else 'latest'
                model.save_networks(save_suffix)
        if epoch % opt.save_epoch_freq == 0:  # cache our model every <save_epoch_freq> epochs
            print('saving the model at the end of epoch %d, iters %d' % (epoch, total_iters))
            model.save_networks('latest')
            model.save_networks(epoch)
        model.update_learning_rate()
